# Singularity

This repo contains commits for the definition file to construct the singularity image that I use in (pretty much) every single project.

To easily construct singularity images go to: https://cloud.sylabs.io/builder 

To pull images from the remote builder this is what should work (but it doesn't because sylabs breaks the connection, therefore I need to download image to local and upload to minerva)

1. Check the proxies using 
cat /etc/environment
http_proxy="http://172.28.7.1:3128/"
https_proxy="https://172.28.7.1:3128/"
ftp_proxy="http://172.28.7.1:3128/"
rsync_proxy="http://172.28.7.1:3128/"
no_proxy="localhost,127.0.0.1"

2. https_proxy="http://172.28.7.1:3128/" singularity pull --arch amd64 library://juditperala/<name_of_the_image_here>
